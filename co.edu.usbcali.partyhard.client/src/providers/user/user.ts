import { Injectable } from '@angular/core';
import { Song } from '../../models/Song';
import { Http, Headers } from '@angular/http';
import { CONFIG } from '../config';
import { Device } from '@ionic-native/device';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  uuid : any;
  constructor(public http: Http,
    public device : Device) {
    console.log('Hello UserProvider Provider');
  }

  setUUID(){
    this.uuid = this.device.uuid;
  }

  insert(uuid){
    let url = CONFIG.server_services_url + "User/insert";
    let headers  = new Headers();
    let json = {
      uuid: uuid,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

  registerVote(uuid, id_song){
    let url = CONFIG.server_services_url + "User/registerVote";
    let headers  = new Headers();
    let json = {
      id_song: id_song,
      uuid: uuid,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

  getRegister(id_song, uuid){
    let url = CONFIG.server_services_url + "User/getRegister";
    let headers  = new Headers();
    let json = {
      id_song: id_song,
      uuid: uuid,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
    //return this.http.get(CONFIG.server_services_url + "Songs/get/"+id);
  }

  delete(uuid){
    let url = CONFIG.server_services_url + "User/delete";
    let headers  = new Headers();
    let json = {
      uuid: "uuid = '"+ uuid + "'",
    }
    console.log(json.uuid);
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

  jsonToUrl(json){
    return Object.keys(json).map(key =>encodeURIComponent(key) + '=' + encodeURIComponent(json[key])).join('&');
  }

}
