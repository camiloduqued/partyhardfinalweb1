let ip = '192.168.0.10/';
var server_url = 'http://' + ip;

export const CONFIG = {
    server_url : server_url,
    server_services_url : server_url + 'PartyHardFinalWeb1/co.edu.usbcali.partyhard.server/',
    server_files_url : server_url + 'PartyHardFinalWeb1/co.edu.usbcali.partyhard.server/public/',
}