import { Injectable } from '@angular/core';
import { Song } from '../../models/Song';
import { Http, Headers } from '@angular/http';
import { CONFIG } from '../config';

/*
  Generated class for the SongsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SongsProvider {

  songs: Song[]; 

  constructor(public http: Http) {
    
  }

  get(id) {
    return this.http.get(CONFIG.server_services_url + "Songs/get/"+id);
  }

  getAll() {
    return this.http.get(CONFIG.server_services_url + "Songs/get");
  }

  getInPlaylist() {
    return this.http.get(CONFIG.server_services_url + "Songs/getInPlaylist");
  }

  getOutPlaylist(query = null) {
    if(query == null){
      return this.http.get(CONFIG.server_services_url + "Songs/getOutPlaylist");
    } else {
      return this.http.get(CONFIG.server_services_url + "Songs/getOutPlaylist/" + query);
    }
  }

  updateInPlaylist(id, value){
    let url = CONFIG.server_services_url + "Songs/updateInPlaylist";
    let headers  = new Headers();
    let json = {
      id: id,
      isInPlaylist: value,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

  vote(id, value){
    let url = CONFIG.server_services_url + "Songs/vote";
    let headers  = new Headers();
    let json = {
      id: id,
      vote: value,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

  jsonToUrl(json){
    return Object.keys(json).map(key =>encodeURIComponent(key) + '=' + encodeURIComponent(json[key])).join('&');
  }
}
