import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SongsProvider } from '../../providers/providers';
import { CONFIG } from '../../providers/config';

/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage {

  songs : any[] = null;
  config = CONFIG;
  query : string = null;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public songsProvider : SongsProvider) {

      this.songsProvider.getOutPlaylist().subscribe(
        data => {this.songs = data.json()},
        error => console.log(error),
        //() => console.log('holi')
      );
      /*
      setInterval(() => {         
        this.songsProvider.getOutPlaylist().subscribe(
          data => {this.songs = data.json()},
          error => console.log(error),
          //() => console.log('holi')
        );
      }, 1000);
      */
      setInterval(() => {
        this.songsProvider.getOutPlaylist(this.query).subscribe(
          data => {
            //console.log(data.json());
            if(data.json() && this.songs){
              //****
              this.songs.forEach((currentSong, index) => {
                let aux = true;
                for(let newSong of data.json()){
                  if(newSong.id == currentSong.id){
                    aux = false;
                    break;
                  }
                }
                if(aux){
                  this.songs.splice(index, 1);
                }
              });
              //console.log(data.json());
              //****
              let songsToAdd = [];
              for(let newSong of data.json()){
                let aux = true;
                for(let currentSong of this.songs){
                  if(newSong.id == currentSong.id){
                    aux = false;
                    break;
                  }
                }
                if(aux){
                  songsToAdd.push(newSong);
                }
              }
              
              if (songsToAdd && songsToAdd.length>0) {
                for(let songToAdd of songsToAdd){
                  this.songs.push(songToAdd);
                }
              }
            }
            if(!this.songs){
              this.songs = data.json();
            }
            if(!data.json()){
              this.songs = null;
            }
          },
          error => console.log('holi error' , error),
          () => {
            this.sortSongs();
          }
        );

      }, 500);
      
  }

  addToPlaylist(song){
    this.songsProvider.updateInPlaylist(song.id, 1).subscribe(
      data => //console.log(data),
      error => console.log(error),
    );
  }

  sortSongs(){
    function compareStrings(a, b) {
      // Assuming you want case-insensitive comparison
      a = a.toLowerCase();
      b = b.toLowerCase();
    
      return (a < b) ? -1 : (a > b) ? 1 : 0;
    }
    
    this.songs.sort(function(a, b) {
      return compareStrings(a.title, b.title);
    })
  }

  setQuery(evt : any){
    if(evt.target.value && evt.target.value.trim() != ''){
      this.query = evt.target.value.trim();
    } else {
      this.query = null;
    }
    console.log(this.query);
  }

}
