import { Component } from '@angular/core';
import { LibraryPage } from '../library/library';
import { PlaylistPage } from '../playlist/playlist';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PlaylistPage;
  tab2Root = LibraryPage;

  constructor() {

  }
}
