import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SongsProvider } from '../../providers/providers';
import { CONFIG } from '../../providers/config';

import { ItemSliding } from 'ionic-angular/components/item/item-sliding';
import { Provider } from '@angular/core/src/di/provider';
import { UserProvider } from '../../providers/user/user';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

/**
 * Generated class for the PlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-playlist',
  templateUrl: 'playlist.html',
})
export class PlaylistPage {

  songs : any[] = null;
  config = CONFIG;
  slidingDrag : any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public songsProvider : SongsProvider,
    public userProvider : UserProvider,
    private toastCtrl : ToastController) {
    this.songsProvider.getInPlaylist().subscribe(
      data => {this.songs = data.json()},
      error => console.log(error),
      //() => console.log('holi')
    );

    setInterval(() => {
      this.songsProvider.getInPlaylist().subscribe(
        data => {
          console.log(data.json());
          if(data.json() && this.songs){
            //****
            this.songs.forEach((currentSong, index) => {
              let aux = true;
              for(let newSong of data.json()){
                if(newSong.id == currentSong.id){
                  aux = false;
                  break;
                }
              }
              if(aux){
                this.songs.splice(index, 1);
              }
            });
            console.log(data.json());
            //****
            let songsToAdd = [];
            for(let newSong of data.json()){
              let aux = true;
              for(let currentSong of this.songs){
                if(newSong.id == currentSong.id){
                  aux = false;
                  break;
                }
              }
              if(aux){
                songsToAdd.push(newSong);
              }
            }
            
            if (songsToAdd && songsToAdd.length>0) {
              for(let songToAdd of songsToAdd){
                this.songs.push(songToAdd);
              }
            }
          }
          if(!this.songs){
            this.songs = data.json();
          }
          if(!data.json()){
            this.songs = null;
          }
        },
        error => console.log('holi error' , error),
        //() => console.log('holi')
      );
    }, 1000);
  }

  slideDrag(item){
    this.slidingDrag=item.getSlidingPercent();
  }

  validateVote(song, item){
    if (this.slidingDrag >=1 || this.slidingDrag <=-1) {
      this.userProvider.getRegister(song.id, this.userProvider.uuid).subscribe(
        data => {
          if(data.text() == 'null'){
            this.vote(song, item);
          } else {
            let toast = this.toastCtrl.create({
              message: 'Lo sentimos, usted ya votó por esta canción.',
              duration: 3000,
              position: 'bottom'
            });
            item.close();
            toast.present();
          }
        },
        error => console.log(error),
      );
    }
  }

  vote(song,item){
    //para el voto hacia la izquierda - Negativo
    
    if (this.slidingDrag >=1) {
      console.log("Negativo");
      //codigo para votar
      this.registerVote(this.userProvider.uuid, song.id).then(()=>{
        this.songsProvider.vote(song.id, 0).subscribe(
          data => {
            console.log(data.text());
          },
          error => {
            console.log(error);
          },
          () => {
            
          }
        );
      }); 
    }
    //para el voto hacia la derecha - Positivo
    if (this.slidingDrag <=-1) {
      console.log("Positivo");
      //codigo para votar
      this.registerVote(this.userProvider.uuid, song.id).then((res)=>{
        console.log(res);
        this.songsProvider.vote(song.id, 1).subscribe(
          data => {
            console.log(data.text());
          },
          error => {
            console.log(error);
          },
          () => {
          }
        );
      });
    }
    item.close();

  }

  registerVote(uuid, id_song){
    return new Promise((resolve, reject)=>{
      this.userProvider.registerVote(this.userProvider.uuid, id_song).subscribe(
        data => resolve(data.text()),
        error => reject(error),
      );
    });
    
  }

}
