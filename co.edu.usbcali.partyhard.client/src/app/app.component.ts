import { Component } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Device } from '@ionic-native/device';
import { UserProvider } from '../providers/user/user';


//@ViewChild('content') nav : Nav;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private device : Device,
    private userProvider : UserProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      console.log(device.uuid);
      userProvider.insert(device.uuid).subscribe(
        data => console.log(data),
        error => console.log(error)
      );
      this.userProvider.setUUID();
    });
  }

  logout(){
    this.userProvider.delete(this.device.uuid).subscribe(
      data => console.log(data.text()),
      error => console.log(error),
      () => {
        this.closeApp();
      }
    );
  }

  closeApp(){
    this.platform.exitApp(); // stops the app
    window.close();
  }
}
