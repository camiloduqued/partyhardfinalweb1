import {Element} from "./element";

export class Player extends Element{
    
    title: string;
    artist: string;
    album: string;
    cover: string;
    src: string;
    params: object;
    domElement: any;

    constructor(title:string,artist:string,album:string,cover:string,src:string,params) {
        super();
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.cover = cover;
        this.src = src;
        
        this.initDomElement();
    }

    initDomElement(){

        let el = document.createElement("div");
        el.id = "player";
        //Componentes
        let songInfo = document.createElement("div");
        songInfo.className = "songInfo";

        let cover = document.createElement("img");
        cover.src = this.cover;

        let songTitle = document.createElement("div");
        songTitle.className = "songTitle";

        let title = document.createElement("div");
        title.className = "title";
        title.innerHTML = this.title;

        let aa = document.createElement("div");
        aa.className = "artistAlbum";
        aa.innerHTML = this.artist+" - "+this.album;

        songTitle.appendChild(title);
        songTitle.appendChild(aa);

        songInfo.appendChild(cover);
        songInfo.appendChild(songTitle);

        el.appendChild(songInfo);
        //Audio tag
        let audioTag = document.createElement("audio");
        audioTag.src = this.src;
        audioTag.preload = "auto";

        el.appendChild(audioTag);
        //Controls
        let controls = document.createElement("div");
        controls.className = "controls";
        let bwBtn:any = document.createElement("ion-icon");
        bwBtn.name = "skip-backward";
        let playBtn: any = document.createElement("ion-icon");
        playBtn.name = "play";
        let fwBtn: any = document.createElement("ion-icon");
        fwBtn.name = "skip-forward";
        controls.appendChild(bwBtn);
        controls.appendChild(playBtn);
        controls.appendChild(fwBtn);

        el.appendChild(controls);
        this.domElement = el;
    }
    
}