export class Element {
    
    domElement: any;

    renderTo(element){
        element.appendChild( this.domElement );
    }
    
}