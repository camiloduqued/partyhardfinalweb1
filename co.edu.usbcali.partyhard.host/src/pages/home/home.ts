import { Component, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { Player } from '../../models/Player';
import { SongsProvider } from '../../providers/providers';
import { CONFIG } from '../../providers/config';
import { NativeAudio } from '@ionic-native/native-audio';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Content) content: Content;  

  playlist: any[] = null;
  playing: any = false;
  config = CONFIG;
  playingSong: any = null;
  player : any = null;
  currentProgress : number = 0;
  currentVolume : number = 0;


  constructor(public navCtrl: NavController, 
    private songsProvider: SongsProvider,
    private nativeAudio : NativeAudio) {
      /*
      this.songsProvider.getInPlaylist().subscribe(
        data => {this.playlist = data.json()},
        error => console.log(error),
        () => {
          if(this.playlist && this.playlist.length >= 0){
            this.playingSong = this.playlist[0];
            this.content.resize();
          }
        }
      );

      setInterval(() => {         
        this.songsProvider.getInPlaylist().subscribe(
          data => {this.playlist = data.json()},
          error => console.log(error),
          () => {this.content.resize();}
        );
      }, 1000);
      */

      setInterval(()=>{
        let audio = document.getElementsByTagName('audio')[0];
        this.currentVolume = audio.volume * 100;
        console.log(this.currentVolume);
      }, 1000);

      this.songsProvider.getInPlaylist().subscribe(
        data => {this.playlist = data.json()},
        error => console.log(error),
        () => {
          if(this.playlist && this.playlist.length >= 0){
            this.playingSong = this.playlist[0];
            this.content.resize();
          }
        }
      );
  
      setInterval(() => {
        this.songsProvider.getInPlaylist().subscribe(
          data => {
            //console.log(data.json());
            if(data.json() && this.playlist){
              //****
              this.playlist.forEach((currentSong, index) => {
                let aux = true;
                for(let newSong of data.json()){
                  if(newSong.id == currentSong.id){
                    aux = false;
                    break;
                  }
                }
                if(aux){
                  this.playlist.splice(index, 1);
                }
              });
              //console.log(data.json());
              //****
              let songsToAdd = [];
              for(let newSong of data.json()){
                let aux = true;
                for(let currentSong of this.playlist){
                  if(newSong.id == currentSong.id){
                    aux = false;
                    break;
                  }
                }
                if(aux){
                  songsToAdd.push(newSong);
                }
              }
              
              if (songsToAdd && songsToAdd.length>0) {
                for(let songToAdd of songsToAdd){
                  this.playlist.push(songToAdd);
                }
              }
            }
            if(!this.playlist){
              this.playlist = data.json();
            }
            if(!data.json()){
              this.playlist = null;
            }
          },
          error => console.log('holi error' , error),
          () => {this.content.resize();}
        );
      }, 1000);
  }

  playSong(song){
    this.player = document.getElementById("song-audio");
    this.playingSong = song;
    this.playing = true;
    this.player.addEventListener('canplay', (d)=>{
      this.player.play();
    }, false);
    //this.player.load();
    
    console.log('play');
  }

  playpause(e){
    this.player = document.getElementById("song-audio");
    if(!this.playing){ 
      this.player.play();
      this.playing = true;
    }else{ 
      this.player.pause();
      this.playing = false;
    }
  }
  backward(){
    if(this.playingSong){
      let auxIndex = -1;
      this.playlist.forEach((element, index)=>{
        if(this.playingSong.id == element.id){
          auxIndex = index;
          return true;
        }
      });
      if(auxIndex >0){
        this.playSong(this.playlist[auxIndex-1]);
      }
    }
  }
  forward(){
    if(this.playingSong){
      let auxIndex = -1;
      this.playlist.forEach((element, index)=>{
        if(this.playingSong.id == element.id){
          auxIndex = index;
          return true;
        }
      });
      if(auxIndex < this.playlist.length-1){
        this.playSong(this.playlist[auxIndex+1]);
      }
    }
  }

  updateProgress(audioTag){
    this.currentProgress = (audioTag.currentTime/audioTag.duration)*100;
    
  }

  volumeDown(){
    if(this.currentVolume >= 10){
      this.currentVolume -= 10;
      document.getElementsByTagName('audio')[0].volume -= 0.1;
    }
  }

  volumeUp(){
    if(this.currentVolume <= 90){
      this.currentVolume += 10;
      document.getElementsByTagName('audio')[0].volume += 0.1;
    }
  }

}
