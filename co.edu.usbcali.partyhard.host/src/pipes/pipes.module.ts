import { NgModule } from '@angular/core';
import { EscapeHtmlPipe } from './keep-html/keep-html';
@NgModule({
	declarations: [EscapeHtmlPipe],
	imports: [],
	exports: [EscapeHtmlPipe]
})
export class PipesModule {}
