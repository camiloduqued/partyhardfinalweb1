-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-11-2017 a las 05:04:49
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `partyhard`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `song`
--

CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `artist` varchar(45) NOT NULL,
  `album` varchar(45) NOT NULL,
  `src` varchar(128) NOT NULL,
  `cover` varchar(128) NOT NULL,
  `pos` int(11) NOT NULL,
  `isInPlaylist` tinyint(1) NOT NULL DEFAULT '0',
  `positiveVotes` int(11) NOT NULL,
  `negativeVotes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `song`
--

INSERT INTO `song` (`id`, `title`, `artist`, `album`, `src`, `cover`, `pos`, `isInPlaylist`, `positiveVotes`, `negativeVotes`) VALUES
(1, 'TNT', 'ACDC', 'TNT', 'public/songs/1.mp3', 'public/covers/1.jpg', 0, 1, 0, 0),
(2, 'Going Under', 'Barco', 'Going Under', 'public/songs/2.mp3', 'public/covers/2.jpg', 1, 0, 0, 0),
(5, 'Knights of Cydonia', 'Muse', 'Knights of Cydonia - singler', 'public/songs/5.mp3', 'public/covers/5.jpg', 2, 0, 0, 0),
(6, 'Inmigrant_Song', 'Led zeppelin', 'Inmigrant song - singer', 'public/songs/6.mp3', 'public/covers/6.jpg', 3, 0, 0, 0),
(7, 'Mr. Brightside', 'The killers', 'Mr. Brightside - singer', 'public/songs/7.mp3', 'public/covers/7.jpg', 4, 1, 0, 0),
(8, 'Shoot to thrill', 'AC - DC', 'Shoot to thrill - singer', 'public/songs/8.mp3', 'public/covers/8.jpg', 5, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `uuid` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`uuid`) VALUES
('fe568f422d91b2a2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_votes`
--

CREATE TABLE `user_votes` (
  `id` int(11) NOT NULL,
  `uuid` varchar(150) NOT NULL,
  `id_song` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uuid`);

--
-- Indices de la tabla `user_votes`
--
ALTER TABLE `user_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_uuid` (`uuid`),
  ADD KEY `fk_id_song` (`id_song`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `song`
--
ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `user_votes`
--
ALTER TABLE `user_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `user_votes`
--
ALTER TABLE `user_votes`
  ADD CONSTRAINT `fk_id_song` FOREIGN KEY (`id_song`) REFERENCES `song` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_uuid` FOREIGN KEY (`uuid`) REFERENCES `user` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
