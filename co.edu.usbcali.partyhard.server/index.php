<?php
    
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


    $url = filter_input(INPUT_GET, "url");
    $url = is_null($url) ? "Index/index" : $url;
    $url = explode("/",$url);
    
    $controller = (isset($url[0]) && !is_null($url[0])) ? $url[0].'Controller' : 'IndexController';
    $method = (isset($url[1])) ? $url[1] : 'index';
    $params = (isset($url[2])) ? $url[2] : null;
    
    require './loader.php';
    
    $controller = new $controller();
    
    if(method_exists($controller, $method)){
        if(isset($params)){
            $controller->{$method}($params);
        }else{
            $controller->$method();
        }
        
    }
    